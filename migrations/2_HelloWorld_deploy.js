const HelloWorld = artifacts.require("HelloWorld");

module.exports = function (deployer, network, accounts) {
    deployer.deploy(HelloWorld).then(function (instance) {
        instance.setMessage("KB was here", { value: 100000, from: accounts[0] }).then(function () {
            console.log('Success')
        }).catch(function (err) {
            console.log(`Error is: ${err}`);
        });
    });
};
