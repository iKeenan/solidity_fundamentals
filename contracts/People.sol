pragma solidity 0.7.5;

contract People {
    struct Person {
        string name;
        uint age;
        uint height;
        bool senior;
    }
    address public owner;
    

    modifier onlyOwner(){
        require(msg.sender == owner);
        _;
    }
    mapping(address => Person) private people;
    address[] private creators;

    function createPerson(
        string memory name,
        uint256 age,
        uint256 height
    ) public onlyOwner {
        require(age < 150, "Age needs to be under 150");
        require(msg.value >= 1 ether);
        balance += msg.value;

        Person memory newPerson = Person(name, age, height);        

        if (age >= 65) {
            newPerson.senior = true;
        } else {
            newPerson.senior = false;
        }
    }
    
    function insert(Person memory newPerson) private {
        address creator = msg.sender;
        people[creator] = newPerson;
    }

    function getPerson(uint _index) public view returns(string memory name, uint age, uint height, bool senior) public {
        Person memory personToReturn =  people[_index];
        return (personToReturn.age, personToReturn.name);
    }

    function deletePerson(address creator) public onlyOwner {
        string memory name = people[name].name;
        bool senior = people[creator].senior;

        delete people[creator];
        assert(people[creator].age == 0);
        emit personDeleted(name, senior, owner);
    }

    function getCreator(uint256 index) public view onlyOwner returns (address) {
        return creators[index];
    }
}
