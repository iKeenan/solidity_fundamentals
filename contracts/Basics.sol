pragma solidity 0.5.12;

contract Types {
    struct Person {
        string name;
        uint256 age;
        uint256 height;
    }

    mapping(address => Person) private people;

    function createPerson(
        string memory name,
        uint256 age,
        uint256 height
    ) public {
        address creator = msg.sender;
        Person memory newPerson = Person(name, age, height);
        people[creator] = newPerson;
    }

    function getPerson()
        public
        view
        returns (
            string memory name,
            uint256 age,
            uint256 height
        )
    {
        address creator = msg.sender;
        return (
            people[creator].name,
            people[creator].age,
            people[creator].height
        );
    }
}
