const HelloWorld = artifacts.require("HelloWorld");

contract("HelloWorld", async function () {
    it("should init correctly", async function () {
        let instance = await HelloWorld.deployed();
        let message = await instance.getMessage();
        assert(message === "KB was here",
            "Message should be KB was here");
    });

    it("should set the message correctly", async function () {
        let instance = await HelloWorld.deployed();
        let input = "Testing";
        await instance.setMessage(input);
        let message = await instance.getMessage();

        assert(message === input,
            `Message should be ${input}`);
    });
});